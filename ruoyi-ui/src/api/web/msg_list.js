import request from '@/utils/request'

// 查询消息记录列表列表
export function listMsg_list(query) {
  return request({
    url: '/web/msg_list/list',
    method: 'get',
    params: query
  })
}

// 查询消息记录列表详细
export function getMsg_list(id) {
  return request({
    url: '/web/msg_list/' + id,
    method: 'get'
  })
}

// 新增消息记录列表
export function addMsg_list(data) {
  return request({
    url: '/web/msg_list',
    method: 'post',
    data: data
  })
}

// 修改消息记录列表
export function updateMsg_list(data) {
  return request({
    url: '/web/msg_list',
    method: 'put',
    data: data
  })
}

// 删除消息记录列表
export function delMsg_list(id) {
  return request({
    url: '/web/msg_list/' + id,
    method: 'delete'
  })
}
