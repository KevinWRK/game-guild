import request from '@/utils/request'

// 查询消息记录表列表
export function listMsg_record(query) {
  return request({
    url: '/web/msg_record/list',
    method: 'get',
    params: query
  })
}

// 查询消息记录表详细
export function getMsg_record(id) {
  return request({
    url: '/web/msg_record/' + id,
    method: 'get'
  })
}

// 新增消息记录表
export function addMsg_record(data) {
  return request({
    url: '/web/msg_record',
    method: 'post',
    data: data
  })
}

// 修改消息记录表
export function updateMsg_record(data) {
  return request({
    url: '/web/msg_record',
    method: 'put',
    data: data
  })
}

// 删除消息记录表
export function delMsg_record(id) {
  return request({
    url: '/web/msg_record/' + id,
    method: 'delete'
  })
}
