import request from '@/utils/request'

// 查询游戏列表
export function listGame(query) {
  return request({
    url: '/web/game/list',
    method: 'get',
    params: query
  })
}

// 查询游戏详细
export function getGame(id) {
  return request({
    url: '/web/game/' + id,
    method: 'get'
  })
}

// 新增游戏
export function addGame(data) {
  return request({
    url: '/web/game',
    method: 'post',
    data: data
  })
}

// 修改游戏
export function updateGame(data) {
  return request({
    url: '/web/game',
    method: 'put',
    data: data
  })
}

// 删除游戏
export function delGame(id) {
  return request({
    url: '/web/game/' + id,
    method: 'delete'
  })
}
