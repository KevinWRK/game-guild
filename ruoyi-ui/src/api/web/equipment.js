import request from '@/utils/request'

// 查询游戏装备列表
export function listEquipment(query) {
  return request({
    url: '/web/equipment/list',
    method: 'get',
    params: query
  })
}

// 查询游戏装备详细
export function getEquipment(id) {
  return request({
    url: '/web/equipment/' + id,
    method: 'get'
  })
}

// 新增游戏装备
export function addEquipment(data) {
  return request({
    url: '/web/equipment',
    method: 'post',
    data: data
  })
}

// 修改游戏装备
export function updateEquipment(data) {
  return request({
    url: '/web/equipment',
    method: 'put',
    data: data
  })
}

// 删除游戏装备
export function delEquipment(id) {
  return request({
    url: '/web/equipment/' + id,
    method: 'delete'
  })
}
