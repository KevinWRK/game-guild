package com.ruoyi.common.constant;

/**
 * 返回状态码
 * 
 * @author ruoyi
 */
public class HttpStatus {
    /**
     * 操作成功
     */
    public static final int SUCCESS = 200;

    /**
     * 对象创建成功
     */
    public static final int CREATED = 201;

    /**
     * 请求已经被接受
     */
    public static final int ACCEPTED = 202;

    /**
     * 操作已经执行成功，但是没有返回数据
     */
    public static final int NO_CONTENT = 204;

    /**
     * 资源已被移除
     */
    public static final int MOVED_PERM = 301;

    /**
     * 重定向
     */
    public static final int SEE_OTHER = 303;

    /**
     * 资源没有被修改
     */
    public static final int NOT_MODIFIED = 304;

    /**
     * 参数列表错误（缺少，格式不匹配）
     */
    public static final int BAD_REQUEST = 400;

    /**
     * 未授权
     */
    public static final int UNAUTHORIZED = 401;

    /**
     * 访问受限，授权过期
     */
    public static final int FORBIDDEN = 403;

    /**
     * 资源，服务未找到
     */
    public static final int NOT_FOUND = 404;

    /**
     * 不允许的http方法
     */
    public static final int BAD_METHOD = 405;

    /**
     * 资源冲突，或者资源被锁
     */
    public static final int CONFLICT = 409;

    /**
     * 不支持的数据，媒体类型
     */
    public static final int UNSUPPORTED_TYPE = 415;

    /**
     * 系统内部错误
     */
    public static final int ERROR = 500;

    /**
     * 接口未实现
     */
    public static final int NOT_IMPLEMENTED = 501;

    /**
     * token失效
     */
    public static final int VERIFY_TOKEN_FAILURE = 10001;

    /**
     * 余额不足
     */
    public static final int LACK_OF_BALANCE = 10002;

    /**
     * 未查询到数据
     */
    public static final int QUERY_EMPTY = 10003;

    /**
     * 参数错误（格式不匹配）
     */
    public static final int PARAM_ERROR = 10004;

    /**
     * 参数不能为空
     */
    public static final int PARAM_NOT_NULL = 20001;

    /**
     * 请使用邮箱注册方式注册
     */
    public static final int PLEASE_E_MALL_REGISTER = 20002;

    /**
     * 当前账号已被注册
     */
    public static final int CURRENT_ACCOUNT_IS_REGISTERED = 20003;

    /**
     * 验证码已失效
     */
    public static final int VERIFICATION_CODE_IS_INVALID = 20004;
    /**
     * 验证码错误
     */
    public static final int VERIFICATION_CODE_ERROR = 20005;
    /**
     * 当前手机号已被注册
     */
    public static final int PHONE_NUMBER_REGISTERED = 20006;
    /**
     * 当前邮箱已被注册
     */
    public static final int EMAIL_REGISTERED = 20007;
    /**
     * 推荐码无效
     */
    public static final int INVALID_RECOMMENDATION_CODE = 20008;
    /**
     * 当前用户未注册
     */
    public static final int USER_NOT_REGISTERED = 20010;
    /**
     * 密码错误
     */
    public static final int PASSWORD_ERROR = 20011;

    /**
     * 当前账号未绑定手机
     */
    public static final int ACCOUNT_NOT_BOUND_MOBILE_PHONE = 20013;
    /**
     * 当前账号未绑定邮箱
     */
    public static final int ACCOUNT_NOT_BOUND_EMAIL = 20015;
    /**
     * 当前账号尚未通过谷歌验证
     */
    public static final int NOT_VERIFIED_BY_GOOGLE = 20016;
    /**
     * 谷歌动态码输入错误
     */
    public static final int GOOGLE_DYNAMIC_CODE_ERROR = 20017;
    /**
     * 谷歌验证码失效
     */
    public static final int GOOGLE_VERIFICATION_INVALID = 20018;
    /**
     * 账户余额不足
     */
    public static final int INSUFFICIENT_FUNDS = 20019;
    /**
     * 插入数据失败
     */
    public static final int INSERT_DATA_FAIL = 20020;
    /**
     * 修改数据失败
     */
    public static final int UPDATE_DATA_FAIL = 20021;
    /**
     * 删除数据失败
     */
    public static final int DELETE_DATA_FAIL = 20022;
    /**
     * 订单不存在
     */
    public static final int ORDER_NOT_EXIST = 20023;
    /**
     * 当前订单状态不能被改变
     */
    public static final int NOT_UPDATE_ORDER_STATE = 20024;
    /**
     * 手机验证码发送失败
     */
    public static final int PHONE_CODE_SEND_FAIL = 20025;
    /**
     * 邮箱验证码发送失败
     */
    public static final int MAIL_CODE_SEND_FAIL = 20026;
    /**
     * 账号已被注销
     */
    public static final int ACCOUNT_CANCEL = 20027;
    /**
     * 账号已被冻结
     */
    public static final int ACCOUNT_FREEZING = 20028;
    /**
     *  数据不可修改
     */
    public static final int DATA_UNCHANGEABLE = 10005;
    /**
     *  数据重复
     */
    public static final int DATA_REPEAT = 10006;

    /**
     *  修改账户金额失败
     */
    public static final int UPDATE_MONEY_FAIL = 10007;
    /**
     *  修改账户金额失败
     */
    public static final int DEAL_NOT_ALLOW = 10008;

}
