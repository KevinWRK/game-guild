package com.ruoyi.common.constant;

/**
 * 通用常量信息
 * 
 * @author ruoyi
 */
public class Constants
{
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 注册
     */
    public static final String REGISTER = "Register";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";
    
    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";

    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limit:";

    /**
     * 验证码有效期（分钟）
     */
    public static final Integer CAPTCHA_EXPIRATION = 2;

    /**
     * 令牌
     */
    public static final String TOKEN = "token";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";

    /**
     * 用户ID
     */
    public static final String JWT_USERID = "userid";

    /**
     * 用户名称
     */
    public static final String JWT_USERNAME = "sub";

    /**
     * 用户头像
     */
    public static final String JWT_AVATAR = "avatar";

    /**
     * 创建时间
     */
    public static final String JWT_CREATED = "created";

    /**
     * 用户权限
     */
    public static final String JWT_AUTHORITIES = "authorities";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile";

    /**
     * 交易所密码加盐
     */
    public static final String EXCHANGE_PASSWORD_SALT = "exchange";
    /**
     * redis 交易所邮箱验证码 key
     */
    public static final String EXCHANGE_EMAIL_CODE = "exchangeEmailCode:";
    /**
     * redis 交易所手机验证码 key
     */
    public static final String EXCHANGE_MOBILE_CODE = "exchangeMobileCode:";
    /**
     * redis 交易所用户 token key
     */
    public static final String EXCHANGE_TOKEN = "exchangeToken:";

    /**
     * 交易所 sys_conf表  注册协议
     */
    public static final String EXCHANGE_REGISTRATION_AGREEMENT= "registration_agreement";

    /**
     * 交易所 sys_conf表  分享链接
     */
    public static final String EXCHANGE_SHARE_URL= "EXCHANGE_SHARE_URL";

    /**
     * 交易所 sys_conf表  短信平台账号
     */
    public static final String EXCHANGE_SMS_USERNAME = "EXCHANGE_SMS_USERNAME";

    /**
     * 交易所 sys_conf表  短信平台密码
     */
    public static final String EXCHANGE_SMS_PASSWORD= "EXCHANGE_SMS_PASSWORD";

    /**
     * 交易所 sys_conf表  短信内容头
     */
    public static final String EXCHANGE_SMS_CONTENT_HEAD= "EXCHANGE_SMS_CONTENT_HEAD";

    /**
     * 交易所 sys_conf表  短信内容尾
     */
    public static final String EXCHANGE_SMS_CONTENT_TOIL= "EXCHANGE_SMS_CONTENT_TOIL";

    /**
     * 请求头中的token
     */
    public static final String ACCESS_TOKEN = "access_token";

    /**
     * Mysql 消息记录表前缀
     */
    public static final String WEB_MSG_RECORD_PREFIX = "web_msg_record";

    /**
     * 启用
     */
    public static final int ENABLE_ON = 1;

    /**
     * 禁用
     */
    public static final int ENABLE_OFF = 0;

    /** redis 锁到期时间*/
    public static final int REDIS_CACHE_EXPIRE = 20;



}
