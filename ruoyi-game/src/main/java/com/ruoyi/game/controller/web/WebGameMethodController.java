package com.ruoyi.game.controller.web;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.PageUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.WebGame;
import com.ruoyi.system.domain.WebGameEquipment;
import com.ruoyi.system.domain.WebUser;
import com.ruoyi.system.mapper.WebGameEquipmentMapper;
import com.ruoyi.system.service.IWebGameEquipmentService;
import com.ruoyi.system.service.IWebGameService;
import com.ruoyi.system.service.IWebUserService;
import com.ruoyi.system.service.impl.WebUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/web/client/game/method")
public class WebGameMethodController extends BaseController {
    @Autowired
    private IWebGameService webGameService;
    @Autowired
    private IWebGameEquipmentService webGameEquipmentService;
    @Resource
    private WebGameEquipmentMapper webGameEquipmentMapper;
    @Autowired
    private IWebUserService webUserService;

    /**
     * 用户发布游戏装备接口
     */
    @RequestMapping("/publish")
    public AjaxResult publish(WebGameEquipment gameEquipment, HttpServletRequest request) {
        String userId = verifyToken(request);
        //参数判空
        if (StringUtils.isAnyEmpty(
                gameEquipment.getGameName(),
                gameEquipment.getEquipmentName(),
                gameEquipment.getDetails(),
                gameEquipment.getTitle())
                && gameEquipment.getGameId() != null) {
            return new AjaxResult(HttpStatus.PARAM_NOT_NULL, "参数不能为空");
        }
        gameEquipment.setStatus(1L);//默认上架
        gameEquipment.setUserId(Long.parseLong(userId));//UserID

        webGameEquipmentService.insertWebGameEquipment(gameEquipment);
        return AjaxResult.success();
    }

    /**
     * 用户修改装备信息
     */
    @RequestMapping("/update")
    public AjaxResult update(WebGameEquipment gameEquipment, HttpServletRequest request) {
        verifyToken(request);

        //参数判空
        if (StringUtils.isAnyEmpty(gameEquipment.getId())) {
            return new AjaxResult(HttpStatus.PARAM_NOT_NULL, "参数不能为空");
        }

        webGameEquipmentService.updateWebGameEquipment(gameEquipment);
        return AjaxResult.success();
    }

    /**
     * 用户删除装备信息
     */
    @RequestMapping("/delete")
    public AjaxResult delete(WebGameEquipment gameEquipment, HttpServletRequest request) {
        String userId = verifyToken(request);

        //参数判空
        if (StringUtils.isAnyEmpty(gameEquipment.getId())) {
            return new AjaxResult(HttpStatus.PARAM_NOT_NULL, "参数不能为空");
        }

        //判断是否为自己的装备帖
        WebGameEquipment queriedEquipment = webGameEquipmentService.selectWebGameEquipmentById(gameEquipment.getId());
        if (Long.parseLong(userId) != queriedEquipment.getUserId()) {
            return new AjaxResult(5010,"只能删除自己的装备");
        }

        //删除装备
        webGameEquipmentService.deleteWebGameEquipmentById(gameEquipment.getId());
        return AjaxResult.success();
    }

    /**
     * 分页获取游戏信息（可模糊搜索）
     * 添加分页参数
     */
    @RequestMapping("/selectGameList")
    public AjaxResult selectGameList(WebGame webGame) {
        webGame.setStatus(1L);
        PageUtils.startPage();
        List<WebGame> webGames = webGameService.selectWebGameList(webGame);

        //拼接装备数量字段
        WebGameEquipment query = new WebGameEquipment();
        for (WebGame game : webGames) {
            query.setGameId(Long.parseLong(game.getId()));
            game.setEquipmentNum(webGameEquipmentMapper.selectCount(query));
        }

        return AjaxResult.success(webGames);
    }

    /**
     * 分页获取装备信息(可模糊搜索)
     */
    @RequestMapping("/selectGameEquipmentList")
    public AjaxResult selectGameEquipmentList(WebGameEquipment gameEquipment) {

        //查询装备信息，状态为上架
        gameEquipment.setStatus(1L);
        startPage();
        List<WebGameEquipment> webGameEquipments = webGameEquipmentService.selectWebGameEquipmentList(gameEquipment);

        //查询发布者用户头像和昵称
        for (WebGameEquipment equipment : webGameEquipments) {
            WebUser webUser = webUserService.selectWebUserById(equipment.getUserId().toString());
            equipment.setUserHeadImg(webUser.getHeadImg());
            equipment.setUserNickName(webUser.getNickName());
        }

        return AjaxResult.success(webGameEquipments);
    }



}
