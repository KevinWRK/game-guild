package com.ruoyi.game.controller.admin;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.WebMsgList;
import com.ruoyi.system.service.IWebMsgListService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 消息记录列表Controller
 * 
 * @author kevin
 * @date 2022-05-09
 */
@RestController
@RequestMapping("/web/msg_list")
public class WebMsgListController extends BaseController
{
    @Autowired
    private IWebMsgListService webMsgListService;

    /**
     * 查询消息记录列表列表
     */
    @PreAuthorize("@ss.hasPermi('web:msg_list:list')")
    @GetMapping("/list")
    public TableDataInfo list(WebMsgList webMsgList)
    {
        startPage();
        List<WebMsgList> list = webMsgListService.selectWebMsgListList(webMsgList);
        return getDataTable(list);
    }

    /**
     * 导出消息记录列表列表
     */
    @PreAuthorize("@ss.hasPermi('web:msg_list:export')")
    @Log(title = "消息记录列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WebMsgList webMsgList)
    {
        List<WebMsgList> list = webMsgListService.selectWebMsgListList(webMsgList);
        ExcelUtil<WebMsgList> util = new ExcelUtil<WebMsgList>(WebMsgList.class);
        util.exportExcel(response, list, "消息记录列表数据");
    }

    /**
     * 获取消息记录列表详细信息
     */
    @PreAuthorize("@ss.hasPermi('web:msg_list:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(webMsgListService.selectWebMsgListById(id));
    }

    /**
     * 新增消息记录列表
     */
    @PreAuthorize("@ss.hasPermi('web:msg_list:add')")
    @Log(title = "消息记录列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WebMsgList webMsgList)
    {
        return toAjax(webMsgListService.insertWebMsgList(webMsgList));
    }

    /**
     * 修改消息记录列表
     */
    @PreAuthorize("@ss.hasPermi('web:msg_list:edit')")
    @Log(title = "消息记录列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WebMsgList webMsgList)
    {
        return toAjax(webMsgListService.updateWebMsgList(webMsgList));
    }

    /**
     * 删除消息记录列表
     */
    @PreAuthorize("@ss.hasPermi('web:msg_list:remove')")
    @Log(title = "消息记录列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(webMsgListService.deleteWebMsgListByIds(ids));
    }
}
