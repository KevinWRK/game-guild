package com.ruoyi.game.controller.admin;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.WebGame;
import com.ruoyi.system.service.IWebGameService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 游戏Controller
 * 
 * @author kevin
 * @date 2022-05-08
 */
@RestController
@RequestMapping("/web/game")
public class WebGameController extends BaseController
{
    @Autowired
    private IWebGameService webGameService;

    /**
     * 查询游戏列表
     */
    @PreAuthorize("@ss.hasPermi('web:game:list')")
    @GetMapping("/list")
    public TableDataInfo list(WebGame webGame)
    {
        startPage();
        List<WebGame> list = webGameService.selectWebGameList(webGame);
        return getDataTable(list);
    }

    /**
     * 导出游戏列表
     */
    @PreAuthorize("@ss.hasPermi('web:game:export')")
    @Log(title = "游戏", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WebGame webGame)
    {
        List<WebGame> list = webGameService.selectWebGameList(webGame);
        ExcelUtil<WebGame> util = new ExcelUtil<WebGame>(WebGame.class);
        util.exportExcel(response, list, "游戏数据");
    }

    /**
     * 获取游戏详细信息
     */
    @PreAuthorize("@ss.hasPermi('web:game:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(webGameService.selectWebGameById(id));
    }

    /**
     * 新增游戏
     */
    @PreAuthorize("@ss.hasPermi('web:game:add')")
    @Log(title = "游戏", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WebGame webGame)
    {
        return toAjax(webGameService.insertWebGame(webGame));
    }

    /**
     * 修改游戏
     */
    @PreAuthorize("@ss.hasPermi('web:game:edit')")
    @Log(title = "游戏", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WebGame webGame)
    {
        return toAjax(webGameService.updateWebGame(webGame));
    }

    /**
     * 删除游戏
     */
    @PreAuthorize("@ss.hasPermi('web:game:remove')")
    @Log(title = "游戏", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(webGameService.deleteWebGameByIds(ids));
    }
}
