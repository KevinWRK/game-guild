package com.ruoyi.game.controller.admin;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.WebMsgRecord;
import com.ruoyi.system.service.IWebMsgRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 消息记录表Controller
 * 
 * @author kevin
 * @date 2022-05-09
 */
@RestController
@RequestMapping("/web/msg_record")
public class WebMsgRecordController extends BaseController
{
    @Autowired
    private IWebMsgRecordService webMsgRecordService;

    /**
     * 查询消息记录表列表
     */
    @PreAuthorize("@ss.hasPermi('web:msg_record:list')")
    @GetMapping("/list")
    public TableDataInfo list(WebMsgRecord webMsgRecord)
    {
        startPage();
        List<WebMsgRecord> list = webMsgRecordService.selectWebMsgRecordList(webMsgRecord);
        return getDataTable(list);
    }

    /**
     * 导出消息记录表列表
     */
    @PreAuthorize("@ss.hasPermi('web:msg_record:export')")
    @Log(title = "消息记录表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WebMsgRecord webMsgRecord)
    {
        List<WebMsgRecord> list = webMsgRecordService.selectWebMsgRecordList(webMsgRecord);
        ExcelUtil<WebMsgRecord> util = new ExcelUtil<WebMsgRecord>(WebMsgRecord.class);
        util.exportExcel(response, list, "消息记录表数据");
    }

    /**
     * 获取消息记录表详细信息
     */
    @PreAuthorize("@ss.hasPermi('web:msg_record:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(webMsgRecordService.selectWebMsgRecordById(id));
    }

    /**
     * 新增消息记录表
     */
    @PreAuthorize("@ss.hasPermi('web:msg_record:add')")
    @Log(title = "消息记录表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WebMsgRecord webMsgRecord)
    {
        return toAjax(webMsgRecordService.insertWebMsgRecord(webMsgRecord));
    }

    /**
     * 修改消息记录表
     */
    @PreAuthorize("@ss.hasPermi('web:msg_record:edit')")
    @Log(title = "消息记录表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WebMsgRecord webMsgRecord)
    {
        return toAjax(webMsgRecordService.updateWebMsgRecord(webMsgRecord));
    }

    /**
     * 删除消息记录表
     */
    @PreAuthorize("@ss.hasPermi('web:msg_record:remove')")
    @Log(title = "消息记录表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(webMsgRecordService.deleteWebMsgRecordByIds(ids));
    }
}
