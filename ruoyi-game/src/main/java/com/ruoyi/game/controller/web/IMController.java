package com.ruoyi.game.controller.web;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.system.service.impl.IMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/web/client/im")
public class IMController extends BaseController {
    @Autowired
    private IMService imService;

    /**
     * 查询当前用户是否有未读消息（小红点）
     * 返回码 2001有新消息  2002没有新消息
     */
    @RequestMapping("/haveUnread")
    public AjaxResult userHaveUnread(HttpServletRequest request){
        String userId = verifyToken(request);
        return imService.userHaveUnread(Long.parseLong(userId));
    }

    /**
     * 查询聊天好友列表（排序和未读小红点）
     * 需要传分页数据
     * 需要传排序数据 isAsc:desc,orderByColumn:last_time (排序以last_time字段降序排列)
     * 需要传reasonable:false
     */
    @RequestMapping("/chatList")
    public AjaxResult chatList(HttpServletRequest request){
        String userId = verifyToken(request);
        return imService.chatObjList(Long.parseLong(userId));
    }

    /**
     * 单个用户聊天记录（限制：只能查询200条消息记录）
     * @param objUid        聊天对象ID
     * @param beforeOf      查询此时间戳之前消息数据
     *
     */
    @RequestMapping("/singleUserMsgRecord")
    public AjaxResult singleUserMsgRecord(HttpServletRequest request, Long objUid, Long beforeOf){
        String userId = verifyToken(request);
        //限制只能查询200条消息记录
        //todo 判断是否有用
        PageDomain pageDomain = TableSupport.buildPageRequest();
        if (pageDomain.getPageNum() * pageDomain.getPageSize() > 200) {
            return AjaxResult.error(5001,"Data exceeds upper limit");
        }
        return imService.singleUserMsgRecord(Long.parseLong(userId), objUid, beforeOf);
    }

    /**
     * 点击了某个对话框，修改已读状态
     * @param objUid 聊天对象ID
     */
    @RequestMapping("/updateReadStatus")
    public AjaxResult updateReadStatus(HttpServletRequest request, Long objUid){
        String userId = verifyToken(request);
        return imService.updateReadStatus(Long.parseLong(userId), objUid);
    }
}
