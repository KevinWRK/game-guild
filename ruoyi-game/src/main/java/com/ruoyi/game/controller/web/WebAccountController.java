package com.ruoyi.game.controller.web;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.JwtUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.ip.IpUtils;
import com.ruoyi.game.controller.qiniu.QiniuInit;
import com.ruoyi.system.domain.WebMsgList;
import com.ruoyi.system.domain.WebUser;
import com.ruoyi.system.service.IWebMsgListService;
import com.ruoyi.system.service.IWebUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/web/client/account")
public class WebAccountController extends BaseController {
    @Autowired
    private IWebUserService webUserService;

    /**
     * 注册账号
     *
     * @param userInfo   username,mobile,password,fundPassword,parentUid
     */
    @RequestMapping("/signUp")
    public AjaxResult signUp(WebUser userInfo, HttpServletRequest request) {

        //参数判空
        if (StringUtils.isAnyEmpty(userInfo.getPassword(), userInfo.getUsername())) {
            return new AjaxResult(HttpStatus.PARAM_NOT_NULL, "参数不能为空");
        }

        //账号是否被注册了
        WebUser query = new WebUser();
        query.setUsername(userInfo.getUsername());
        if (webUserService.selectWebUserList(query).size() != 0) {
            return new AjaxResult(HttpStatus.CURRENT_ACCOUNT_IS_REGISTERED, "当前账号已被注册");
        }

        //登录密码
        userInfo.setPassword(SecureUtil.md5(userInfo.getPassword() + Constants.EXCHANGE_PASSWORD_SALT));
//        userInfo.setPassword(userInfo.getPassword());

        //资金密码
//        userInfo.setFundPassword(SecureUtil.md5(userInfo.getFundPassword() + Constants.EXCHANGE_PASSWORD_SALT));
//        userInfo.setFundPassword(userInfo.getFundPassword());

        //推荐人
//        if (userInfo.getParentUid() != null && userInfo.getParentUid() != 0) {
//            //判断推荐码是否正确
//            ExchangeUserInfo queryParent = new ExchangeUserInfo();
//            queryParent.setUid(userInfo.getParentUid());
//            List<ExchangeUserInfo> parentInfoList = exchangeUserInfoService.selectExchangeUserInfoList(queryParent);
//            if (parentInfoList.size() == 0) {
//                return new AjaxResult(HttpStatus.INVALID_RECOMMENDATION_CODE, "推荐码无效");
//            }
//            ExchangeUserInfo parentInfo = parentInfoList.get(0);
//            //修改parent的推荐人数
////            parentInfo.setNumberOfPromoters(parentInfo.getNumberOfPromoters() + 1);
////            exchangeUserInfoService.updateExchangeUserInfo(parentInfo);
//            exchangeUserInfoService.updateNumberOfPromoters(parentInfo.getId());
//        }

        //昵称
        userInfo.setNickName(UUID.randomUUID().toString().substring(0,8));
        userInfo.setCreateTime(new Date());
        userInfo.setUpdateTime(new Date());
        userInfo.setAccountStatus(1L);//1正常，2注销，3封号
        userInfo.setAccountType(1L);//1普通账号，2测试账号
        // 注册ip
        userInfo.setRegisterip(IpUtils.getIpAddr(request));
        webUserService.insertWebUser(userInfo);

        return AjaxResult.success();
    }

    /**
     * 账号登录
     * username,password
     */
    @RequestMapping("/login")
    public AjaxResult signIn(WebUser loginUser, HttpServletRequest request) {

        //通过用户名查询用户
        WebUser query = new WebUser();
        query.setUsername(loginUser.getUsername());
        List<WebUser> userInfos = webUserService.selectWebUserList(query);

        //用户未注册
        if (userInfos.size() == 0) {
            return new AjaxResult(HttpStatus.USER_NOT_REGISTERED, "当前用户未注册");
        }

        //密码是否正确
        WebUser userInfo = userInfos.get(0);
        if (!SecureUtil.md5(loginUser.getPassword() + Constants.EXCHANGE_PASSWORD_SALT).equalsIgnoreCase(userInfo.getPassword())) {
            return new AjaxResult(HttpStatus.PASSWORD_ERROR, "密码错误");
        }
//        if (!loginUser.getPassword().equals(userInfo.getPassword())) {
//            return new AjaxResult(HttpStatus.PASSWORD_ERROR, "密码错误");
//        }

        //判断账号状态
        if (userInfo.getAccountStatus() != 1L) {
            return userInfo.getAccountStatus() == 2 ? new AjaxResult(HttpStatus.ACCOUNT_CANCEL, "账号注销") : new AjaxResult(HttpStatus.ACCOUNT_FREEZING, "账号冻结");
        }

        //生成登录token，有效期七天
        String token = JwtUtils.sign(userInfo.getId()+"", userInfo.getPassword());
        redisCache.setCacheObject(Constants.EXCHANGE_TOKEN + userInfo.getId(), token, 7, TimeUnit.DAYS);

        // 修改登录ip
        WebUser update = new WebUser();
        update.setId(userInfo.getId());
        update.setLoginip(IpUtils.getIpAddr(request));
        webUserService.updateWebUser(update);

        //返回参数
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", token);
        jsonObject.put("userInfo", userInfo);
        jsonObject.put("qiniuToken", QiniuInit.qiniuToken);
        return AjaxResult.success(jsonObject);
    }

    /**
     * 注册协议
     */
//    @RequestMapping("/registrationAgreement")
//    public AjaxResult registrationAgreement() {
//        ExchangeSysConf exchangeSysConf = exchangeSysConfService.selectExchangeSysConfById(Constants.EXCHANGE_REGISTRATION_AGREEMENT);
//        return AjaxResult.success(exchangeSysConf);
//    }

    /**
     * 修改密码
     *
     * @param requestUserInfo 用户信息
     * username,password,newPassword
     */
    @RequestMapping("/changePassword")
    public AjaxResult changePassword(WebUser requestUserInfo, String newPassword) {

        //参数不能为空
        if (StringUtils.isAnyEmpty(requestUserInfo.getUsername(), requestUserInfo.getPassword(), newPassword)) {
            return new AjaxResult(HttpStatus.PARAM_NOT_NULL, "参数不能为空");
        }

        //通过账户名查询用户
        WebUser query = new WebUser();
        query.setUsername(requestUserInfo.getUsername());
        List<WebUser> userInfos = webUserService.selectWebUserList(query);
        if (userInfos.size() == 0) {
            return new AjaxResult(HttpStatus.USER_NOT_REGISTERED, "当前用户未注册");
        }

        WebUser userInfo = userInfos.get(0);
        //验证旧密码是否正确
        if (!SecureUtil.md5(requestUserInfo.getPassword() + Constants.EXCHANGE_PASSWORD_SALT).equalsIgnoreCase(userInfo.getPassword())) {
            return new AjaxResult(HttpStatus.PASSWORD_ERROR, "密码验证错误");
        }

        //修改密码
            userInfo.setPassword(SecureUtil.md5(newPassword + Constants.EXCHANGE_PASSWORD_SALT));
//            userInfo.setPassword(newPassword);
        webUserService.updateWebUser(userInfo);

        //todo 修改密码后清除token

        return AjaxResult.success();
    }

    /**
     * 退出登录
     *
     */
    @RequestMapping("/loginOut")
    public AjaxResult loginOut(HttpServletRequest request) {
        String accessToken = request.getHeader(Constants.ACCESS_TOKEN);
        String userId = JwtUtils.getUserId(accessToken);
        redisCache.deleteObject(Constants.EXCHANGE_TOKEN + userId);
        return AjaxResult.success();
    }

    /**
     * 查询用户信息
     */
    @RequestMapping("/userInfo")
    public AjaxResult userInfo(HttpServletRequest request) {
        String userId = verifyToken(request);
        return AjaxResult.success(webUserService.selectWebUserById(userId));
    }

    /**
     * 修改用户信息
     */
    @RequestMapping("/updateUserInfo")
    public AjaxResult updateUserInfo(WebUser webUser,HttpServletRequest request) {
        String userId = verifyToken(request);
        //修改信息
        webUser.setId(Integer.parseInt(userId));
        webUserService.updateWebUser(webUser);

        //返还修改后的信息
        return AjaxResult.success(webUserService.selectWebUserById(userId));
    }

}
