package com.ruoyi.game.controller.admin;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.WebUser;
import com.ruoyi.system.service.IWebUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户信息Controller
 * 
 * @author kevin
 * @date 2022-05-08
 */
@RestController
@RequestMapping("/web/user")
public class WebUserController extends BaseController
{
    @Autowired
    private IWebUserService webUserService;

    /**
     * 查询用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('web:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(WebUser webUser)
    {
        startPage();
        List<WebUser> list = webUserService.selectWebUserList(webUser);
        return getDataTable(list);
    }

    /**
     * 导出用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('web:user:export')")
    @Log(title = "用户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WebUser webUser)
    {
        List<WebUser> list = webUserService.selectWebUserList(webUser);
        ExcelUtil<WebUser> util = new ExcelUtil<WebUser>(WebUser.class);
        util.exportExcel(response, list, "用户信息数据");
    }

    /**
     * 获取用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('web:user:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(webUserService.selectWebUserById(id));
    }

    /**
     * 新增用户信息
     */
    @PreAuthorize("@ss.hasPermi('web:user:add')")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WebUser webUser)
    {
        return toAjax(webUserService.insertWebUser(webUser));
    }

    /**
     * 修改用户信息
     */
    @PreAuthorize("@ss.hasPermi('web:user:edit')")
    @Log(title = "用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WebUser webUser)
    {
        return toAjax(webUserService.updateWebUser(webUser));
    }

    /**
     * 删除用户信息
     */
    @PreAuthorize("@ss.hasPermi('web:user:remove')")
    @Log(title = "用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(webUserService.deleteWebUserByIds(ids));
    }
}
