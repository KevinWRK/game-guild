package com.ruoyi.game.controller.admin;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.WebGameEquipment;
import com.ruoyi.system.service.IWebGameEquipmentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 游戏装备Controller
 * 
 * @author kevin
 * @date 2022-05-08
 */
@RestController
@RequestMapping("/web/equipment")
public class WebGameEquipmentController extends BaseController
{
    @Autowired
    private IWebGameEquipmentService webGameEquipmentService;

    /**
     * 查询游戏装备列表
     */
    @PreAuthorize("@ss.hasPermi('web:equipment:list')")
    @GetMapping("/list")
    public TableDataInfo list(WebGameEquipment webGameEquipment)
    {
        startPage();
        List<WebGameEquipment> list = webGameEquipmentService.selectWebGameEquipmentList(webGameEquipment);
        return getDataTable(list);
    }

    /**
     * 导出游戏装备列表
     */
    @PreAuthorize("@ss.hasPermi('web:equipment:export')")
    @Log(title = "游戏装备", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WebGameEquipment webGameEquipment)
    {
        List<WebGameEquipment> list = webGameEquipmentService.selectWebGameEquipmentList(webGameEquipment);
        ExcelUtil<WebGameEquipment> util = new ExcelUtil<WebGameEquipment>(WebGameEquipment.class);
        util.exportExcel(response, list, "游戏装备数据");
    }

    /**
     * 获取游戏装备详细信息
     */
    @PreAuthorize("@ss.hasPermi('web:equipment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(webGameEquipmentService.selectWebGameEquipmentById(id));
    }

    /**
     * 新增游戏装备
     */
    @PreAuthorize("@ss.hasPermi('web:equipment:add')")
    @Log(title = "游戏装备", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WebGameEquipment webGameEquipment)
    {
        return toAjax(webGameEquipmentService.insertWebGameEquipment(webGameEquipment));
    }

    /**
     * 修改游戏装备
     */
    @PreAuthorize("@ss.hasPermi('web:equipment:edit')")
    @Log(title = "游戏装备", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WebGameEquipment webGameEquipment)
    {
        return toAjax(webGameEquipmentService.updateWebGameEquipment(webGameEquipment));
    }

    /**
     * 删除游戏装备
     */
    @PreAuthorize("@ss.hasPermi('web:equipment:remove')")
    @Log(title = "游戏装备", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(webGameEquipmentService.deleteWebGameEquipmentByIds(ids));
    }
}
