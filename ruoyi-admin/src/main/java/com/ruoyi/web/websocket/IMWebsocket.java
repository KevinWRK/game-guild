package com.ruoyi.web.websocket;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import com.github.pagehelper.util.StringUtil;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.system.service.impl.IMService;
import io.netty.channel.ChannelId;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.timeout.IdleStateEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.yeauty.annotation.*;
import org.yeauty.pojo.Session;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@ServerEndpoint(path = "/imConnect", readerIdleTimeSeconds = "20", port = "8088")
public class IMWebsocket extends BaseController {
    @Autowired
    private IMService imService;

    //保存客户端的连接到CurrentMap中
    public static ConcurrentHashMap<Long,Session> uidAndSessionMap = new ConcurrentHashMap<Long,Session>();
    public static ConcurrentHashMap<ChannelId,Long> channelIdAndUidMap = new ConcurrentHashMap<ChannelId,Long>();
    public static AtomicInteger onlineNum = new AtomicInteger();//在线人数


    @BeforeHandshake    //当有新的连接进入时
    public void handshake(Session session, HttpHeaders headers, @RequestParam String req, @RequestParam MultiValueMap reqMap, @PathVariable String arg, @PathVariable Map pathMap){
        System.out.println("***********有新的连接进入***********");
        //验证连接
        String token = reqMap.get("token").toString();
        token = token.substring(1, token.length()-1);//去掉中括号
        String userId = verifyToken(token);
        //将会话存入Map,并增加在线人数
        uidAndSessionMap.put(Long.parseLong(userId), session);
        channelIdAndUidMap.put(session.id(), Long.parseLong(userId));
        onlineNum.getAndIncrement();
    }

    @OnOpen //有新的websocket连接完成时
    public void onOpen(Session session, HttpHeaders headers, @RequestParam String req, @RequestParam MultiValueMap reqMap, @PathVariable String arg, @PathVariable Map pathMap){

        System.out.println("new connection");
        System.out.println(req);
    }

    @OnClose
    public void onClose(Session session) throws IOException {

        //删除会话,并减少在线人数
        Long userId = channelIdAndUidMap.remove(session.id());
        uidAndSessionMap.remove(userId);
        onlineNum.decrementAndGet();

        System.out.println("one connection closed");
    }

    @OnError
    public void onError(Session session, Throwable throwable) {

        //删除会话,并减少在线人数
        Long userId = channelIdAndUidMap.remove(session.id());
        uidAndSessionMap.remove(userId);
        onlineNum.decrementAndGet();

        throwable.printStackTrace();
    }

    @OnMessage //发送的json数据：senderId,receiveId,content
    public void onMessage(Session session, String message) {

        //心跳
        if (StringUtil.isEmpty(message)) {
            return;
        }

        //消息发送时间
        long msgTime = System.currentTimeMillis();

        //取数据
        JSONObject msgMap = new JSONObject(message);
        Long senderId = msgMap.getLong("senderId");
        Long receiveId = msgMap.getLong("receiveId");
        String content = msgMap.getStr("content");

        //发送数据
        Session receiverSession = uidAndSessionMap.get(receiveId);
        if (ObjectUtil.isNotNull(receiverSession)) {
            JSONObject msgJson = new JSONObject();
            msgJson.set("content",content);
            msgJson.set("senderId",senderId);
            msgJson.set("timestamp",msgTime);
            msgJson.set("type","userMsg");
            receiverSession.sendText(msgJson.toString());
        }

        //消息记录保存
        imService.saveMsgRecord(senderId, receiveId, content, msgTime);

        //ack
        JSONObject ackJson = new JSONObject();
        ackJson.set("type","ack");
        session.sendText(ackJson.toString());


        System.out.println(message);
    }

    @OnBinary   //接收到二进制消息时
    public void onBinary(Session session, byte[] bytes) {
        for (byte b : bytes) {
            System.out.println(b);
        }
        session.sendBinary(bytes);
    }

    @OnEvent    //接收到Netty事件时
    public void onEvent(Session session, Object evt) {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
            switch (idleStateEvent.state()) {
                case READER_IDLE:
                    System.out.println("read idle");
                    session.close();//关闭连接
                    Long userId = channelIdAndUidMap.remove(session.id());
                    uidAndSessionMap.remove(userId);
                    onlineNum.decrementAndGet();
                    break;
                case WRITER_IDLE:
                    System.out.println("write idle");
                    break;
                case ALL_IDLE:
                    System.out.println("all idle");
                    break;
                default:
                    break;
            }
        }
    }

}
