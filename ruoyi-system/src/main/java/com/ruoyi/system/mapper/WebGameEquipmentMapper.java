package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WebGameEquipment;

/**
 * 游戏装备Mapper接口
 * 
 * @author kevin
 * @date 2022-05-08
 */
public interface WebGameEquipmentMapper 
{
    /**
     * 查询游戏装备
     * 
     * @param id 游戏装备主键
     * @return 游戏装备
     */
    public WebGameEquipment selectWebGameEquipmentById(String id);

    /**
     * 查询游戏装备列表
     * 
     * @param webGameEquipment 游戏装备
     * @return 游戏装备集合
     */
    public List<WebGameEquipment> selectWebGameEquipmentList(WebGameEquipment webGameEquipment);

    /**
     *  查询数量
     */
    public int selectCount(WebGameEquipment webGameEquipment);

    /**
     * 新增游戏装备
     * 
     * @param webGameEquipment 游戏装备
     * @return 结果
     */
    public int insertWebGameEquipment(WebGameEquipment webGameEquipment);

    /**
     * 修改游戏装备
     * 
     * @param webGameEquipment 游戏装备
     * @return 结果
     */
    public int updateWebGameEquipment(WebGameEquipment webGameEquipment);

    /**
     * 删除游戏装备
     * 
     * @param id 游戏装备主键
     * @return 结果
     */
    public int deleteWebGameEquipmentById(String id);

    /**
     * 批量删除游戏装备
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWebGameEquipmentByIds(String[] ids);
}
