package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WebMsgRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 消息记录表Mapper接口
 * 
 * @author kevin
 * @date 2022-05-09
 */
public interface WebMsgRecordMapper 
{
    /**
     * 查询消息记录表
     * 
     * @param id 消息记录表主键
     * @return 消息记录表
     */
    public WebMsgRecord selectWebMsgRecordById(String id);

    /**
     * 查询消息记录表列表
     * 
     * @param webMsgRecord 消息记录表
     * @return 消息记录表集合
     */
    public List<WebMsgRecord> selectWebMsgRecordList(WebMsgRecord webMsgRecord);

    /**
     * 新增消息记录表
     * 
     * @param webMsgRecord 消息记录表
     * @return 结果
     */
    public int insertWebMsgRecord(WebMsgRecord webMsgRecord);

    /**
     * 修改消息记录表
     * 
     * @param webMsgRecord 消息记录表
     * @return 结果
     */
    public int updateWebMsgRecord(WebMsgRecord webMsgRecord);

    /**
     * 删除消息记录表
     * 
     * @param id 消息记录表主键
     * @return 结果
     */
    public int deleteWebMsgRecordById(String id);

    /**
     * 批量删除消息记录表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWebMsgRecordByIds(String[] ids);
}
