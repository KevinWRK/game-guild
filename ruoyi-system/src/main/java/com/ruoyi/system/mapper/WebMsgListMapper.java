package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WebMsgList;

/**
 * 消息记录列表Mapper接口
 * 
 * @author kevin
 * @date 2022-05-09
 */
public interface WebMsgListMapper 
{
    /**
     * 查询消息记录列表
     * 
     * @param id 消息记录列表主键
     * @return 消息记录列表
     */
    public WebMsgList selectWebMsgListById(String id);

    /**
     * 查询消息记录列表列表
     * 
     * @param webMsgList 消息记录列表
     * @return 消息记录列表集合
     */
    public List<WebMsgList> selectWebMsgListList(WebMsgList webMsgList);

    /**
     * 新增消息记录列表
     * 
     * @param webMsgList 消息记录列表
     * @return 结果
     */
    public int insertWebMsgList(WebMsgList webMsgList);

    /**
     * 修改消息记录列表
     * 
     * @param webMsgList 消息记录列表
     * @return 结果
     */
    public int updateWebMsgList(WebMsgList webMsgList);

    /**
     * 删除消息记录列表
     * 
     * @param id 消息记录列表主键
     * @return 结果
     */
    public int deleteWebMsgListById(String id);

    /**
     * 批量删除消息记录列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWebMsgListByIds(String[] ids);

    /**
     * 查询用户的消息列表
     */
    public List<WebMsgList> selectUserMsgList(Long userId);
}
