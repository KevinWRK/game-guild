package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WebUser;

/**
 * 用户信息Mapper接口
 * 
 * @author kevin
 * @date 2022-05-08
 */
public interface WebUserMapper 
{
    /**
     * 查询用户信息
     * 
     * @param id 用户信息主键
     * @return 用户信息
     */
    public WebUser selectWebUserById(String id);

    /**
     * 查询用户信息列表
     * 
     * @param webUser 用户信息
     * @return 用户信息集合
     */
    public List<WebUser> selectWebUserList(WebUser webUser);

    /**
     * 新增用户信息
     * 
     * @param webUser 用户信息
     * @return 结果
     */
    public int insertWebUser(WebUser webUser);

    /**
     * 修改用户信息
     * 
     * @param webUser 用户信息
     * @return 结果
     */
    public int updateWebUser(WebUser webUser);

    /**
     * 删除用户信息
     * 
     * @param id 用户信息主键
     * @return 结果
     */
    public int deleteWebUserById(String id);

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWebUserByIds(String[] ids);
}
