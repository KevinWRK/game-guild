package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WebGame;

/**
 * 游戏Mapper接口
 * 
 * @author kevin
 * @date 2022-05-08
 */
public interface WebGameMapper 
{
    /**
     * 查询游戏
     * 
     * @param id 游戏主键
     * @return 游戏
     */
    public WebGame selectWebGameById(String id);

    /**
     * 查询游戏列表
     * 
     * @param webGame 游戏
     * @return 游戏集合
     */
    public List<WebGame> selectWebGameList(WebGame webGame);

    /**
     * 新增游戏
     * 
     * @param webGame 游戏
     * @return 结果
     */
    public int insertWebGame(WebGame webGame);

    /**
     * 修改游戏
     * 
     * @param webGame 游戏
     * @return 结果
     */
    public int updateWebGame(WebGame webGame);

    /**
     * 删除游戏
     * 
     * @param id 游戏主键
     * @return 结果
     */
    public int deleteWebGameById(String id);

    /**
     * 批量删除游戏
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWebGameByIds(String[] ids);
}
