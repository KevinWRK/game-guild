package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WebMsgListMapper;
import com.ruoyi.system.domain.WebMsgList;
import com.ruoyi.system.service.IWebMsgListService;

/**
 * 消息记录列表Service业务层处理
 * 
 * @author kevin
 * @date 2022-05-09
 */
@Service
public class WebMsgListServiceImpl implements IWebMsgListService 
{
    @Autowired
    private WebMsgListMapper webMsgListMapper;

    /**
     * 查询消息记录列表
     * 
     * @param id 消息记录列表主键
     * @return 消息记录列表
     */
    @Override
    public WebMsgList selectWebMsgListById(String id)
    {
        return webMsgListMapper.selectWebMsgListById(id);
    }

    /**
     * 查询消息记录列表列表
     * 
     * @param webMsgList 消息记录列表
     * @return 消息记录列表
     */
    @Override
    public List<WebMsgList> selectWebMsgListList(WebMsgList webMsgList)
    {
        return webMsgListMapper.selectWebMsgListList(webMsgList);
    }

    /**
     * 新增消息记录列表
     * 
     * @param webMsgList 消息记录列表
     * @return 结果
     */
    @Override
    public int insertWebMsgList(WebMsgList webMsgList)
    {
        return webMsgListMapper.insertWebMsgList(webMsgList);
    }

    /**
     * 修改消息记录列表
     * 
     * @param webMsgList 消息记录列表
     * @return 结果
     */
    @Override
    public int updateWebMsgList(WebMsgList webMsgList)
    {
        return webMsgListMapper.updateWebMsgList(webMsgList);
    }

    /**
     * 批量删除消息记录列表
     * 
     * @param ids 需要删除的消息记录列表主键
     * @return 结果
     */
    @Override
    public int deleteWebMsgListByIds(String[] ids)
    {
        return webMsgListMapper.deleteWebMsgListByIds(ids);
    }

    /**
     * 删除消息记录列表信息
     * 
     * @param id 消息记录列表主键
     * @return 结果
     */
    @Override
    public int deleteWebMsgListById(String id)
    {
        return webMsgListMapper.deleteWebMsgListById(id);
    }
}
