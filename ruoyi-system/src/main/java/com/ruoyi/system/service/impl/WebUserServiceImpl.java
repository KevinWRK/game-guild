package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WebUserMapper;
import com.ruoyi.system.domain.WebUser;
import com.ruoyi.system.service.IWebUserService;

/**
 * 用户信息Service业务层处理
 * 
 * @author kevin
 * @date 2022-05-08
 */
@Service
public class WebUserServiceImpl implements IWebUserService 
{
    @Autowired
    private WebUserMapper webUserMapper;

    /**
     * 查询用户信息
     * 
     * @param id 用户信息主键
     * @return 用户信息
     */
    @Override
    public WebUser selectWebUserById(String id)
    {
        return webUserMapper.selectWebUserById(id);
    }

    /**
     * 查询用户信息列表
     * 
     * @param webUser 用户信息
     * @return 用户信息
     */
    @Override
    public List<WebUser> selectWebUserList(WebUser webUser)
    {
        return webUserMapper.selectWebUserList(webUser);
    }

    /**
     * 新增用户信息
     * 
     * @param webUser 用户信息
     * @return 结果
     */
    @Override
    public int insertWebUser(WebUser webUser)
    {
        webUser.setCreateTime(DateUtils.getNowDate());
        return webUserMapper.insertWebUser(webUser);
    }

    /**
     * 修改用户信息
     * 
     * @param webUser 用户信息
     * @return 结果
     */
    @Override
    public int updateWebUser(WebUser webUser)
    {
        webUser.setUpdateTime(DateUtils.getNowDate());
        return webUserMapper.updateWebUser(webUser);
    }

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的用户信息主键
     * @return 结果
     */
    @Override
    public int deleteWebUserByIds(String[] ids)
    {
        return webUserMapper.deleteWebUserByIds(ids);
    }

    /**
     * 删除用户信息信息
     * 
     * @param id 用户信息主键
     * @return 结果
     */
    @Override
    public int deleteWebUserById(String id)
    {
        return webUserMapper.deleteWebUserById(id);
    }
}
