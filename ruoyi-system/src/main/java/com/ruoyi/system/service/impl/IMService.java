package com.ruoyi.system.service.impl;

import cn.hutool.json.JSONObject;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.utils.PageUtils;
import com.ruoyi.system.domain.WebMsgList;
import com.ruoyi.system.domain.WebMsgRecord;
import com.ruoyi.system.mapper.WebMsgListMapper;
import com.ruoyi.system.service.IWebMsgListService;
import com.ruoyi.system.service.IWebMsgRecordService;
import org.hibernate.validator.internal.util.Contracts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class IMService {
    @Autowired
    private IWebMsgRecordService webMsgRecordService;
    @Autowired
    private IWebMsgListService webMsgListService;
    @Resource
    private WebMsgListMapper webMsgListMapper;
    /**
     * 保存消息
     * json数据：senderId,receiveId,content
     * 消息记录存入接收者表中
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveMsgRecord(Long senderId, Long receiveId, String content, long mesTime){

        //保存消息记录
        WebMsgRecord msgRecordInsert = new WebMsgRecord();
        msgRecordInsert.setSenderUid(senderId);//发送者ID
        msgRecordInsert.setReceiverUid(receiveId);//接收者ID
        msgRecordInsert.setContent(content);//内容
        msgRecordInsert.setMsgType(1L);//消息类型（1文字，2图片，3文件，4语言）
        msgRecordInsert.setIsRead(0L);//已读（0未读，1已读）
        msgRecordInsert.setSenderDelete(0L);//发送者删除消息记录
        msgRecordInsert.setReceiverDelete(0L);//接收者删除消息记录
//        msgRecordInsert.setTableName(Constants.WEB_MSG_RECORD_PREFIX + (receiveId % 10));//消息记录存入接收者表中
        msgRecordInsert.setCreateTime(new Date(mesTime));
        webMsgRecordService.insertWebMsgRecord(msgRecordInsert);

        //修改WebMsgList未读状态（后期考虑用缓存存储读取状态）---接收者
        WebMsgList queryMsgList = new WebMsgList();
        queryMsgList.setUserId(receiveId);//用户ID
        queryMsgList.setObjUid(senderId);//聊天对象用户ID
        List<WebMsgList> queriedMsgListLis = webMsgListService.selectWebMsgListList(queryMsgList);
        //没有数据则插入，有则修改
        if (queriedMsgListLis.size() == 0) {
            WebMsgList insertWebMsgList = new WebMsgList();
            insertWebMsgList.setUserId(receiveId);//用户ID
            insertWebMsgList.setObjUid(senderId);//聊天对象用户ID
            insertWebMsgList.setUnRead(1L);//是否有未读消息（0无，1有）
            insertWebMsgList.setLastTime(new Date());//最后一条消息的时间
            webMsgListService.insertWebMsgList(insertWebMsgList);
        }else {
            WebMsgList queriedMsgList = queriedMsgListLis.get(0);
            if (queriedMsgList.getUnRead() == 0) {  //状态为已读才修改
                WebMsgList insertWebMsgList = new WebMsgList();
                insertWebMsgList.setId(queriedMsgList.getId());
                insertWebMsgList.setUnRead(1L);
                insertWebMsgList.setLastTime(new Date());//最后一条消息的时间
                webMsgListService.updateWebMsgList(insertWebMsgList);
            }
        }

        //添加WebMsgList数据（后期考虑用缓存存储读取状态）---发送者
        WebMsgList querySenderMsgList = new WebMsgList();
        querySenderMsgList.setUserId(senderId);//用户ID
        querySenderMsgList.setObjUid(receiveId);//聊天对象用户ID
        List<WebMsgList> queriedSenderMsgListLis = webMsgListService.selectWebMsgListList(querySenderMsgList);
        //没有数据则插入
        if (queriedSenderMsgListLis.size() == 0) {
            WebMsgList insertWebMsgList = new WebMsgList();
            insertWebMsgList.setUserId(senderId);//用户ID
            insertWebMsgList.setObjUid(receiveId);//聊天对象用户ID
            insertWebMsgList.setUnRead(0L);//是否有未读消息（0无，1有）
            insertWebMsgList.setLastTime(new Date());//最后一条消息的时间
            webMsgListService.insertWebMsgList(insertWebMsgList);
        }
    }

    /**
     * 查询当前用户是否有未读消息（小红点）
     * @param userId 用户ID
     * 返回码 2001有新消息  2002没有新消息
     */
    public AjaxResult userHaveUnread(Long userId){
        WebMsgList queryMsgList = new WebMsgList();
        queryMsgList.setUserId(userId);//用户ID
        queryMsgList.setUnRead(1L);//是否有未读消息（0无，1有）
        List<WebMsgList> queriedMsgList = webMsgListService.selectWebMsgListList(queryMsgList);
        if (queriedMsgList.size() > 0){
            return new AjaxResult(2001,"There's new message");
        }
        return new AjaxResult(2002,"No new news");
    }

    /**
     * 查询聊天列表（排序和未读小红点）
     * 需要传分页数据
     * 需要传排序数据 isAsc:desc,orderByColumn:last_time (排序以last_time字段降序排列)
     * 需要传reasonable:false
     */
    public AjaxResult chatObjList(Long userId){
        PageUtils.startPage(); // todo 测试分页
        return AjaxResult.success(webMsgListMapper.selectUserMsgList(userId));
    }

    /**
     * 单个用户聊天记录（限制：只能查询200条消息记录）
     * @param userId        用户ID
     * @param objUid        聊天对象ID
     * @param beforeOf      查询此时间戳之前消息数据
     */
    public AjaxResult singleUserMsgRecord(Long userId, Long objUid, Long beforeOf){

//        PageDomain pageDomain = TableSupport.buildPageRequest();
//        int limitSize = pageDomain.getPageNum() * pageDomain.getPageSize();
//
//        //查询作为发送方的消息
//        WebMsgRecord queryASSenderMsgRecord = new WebMsgRecord();
//        queryASSenderMsgRecord.setSenderUid(userId);//发送者ID
//        queryASSenderMsgRecord.setReceiverUid(objUid);//接收者ID
//        //添加查询时间条件 todo 测试按时间查询是否正常
//        if (beforeOf != null && beforeOf != 0) {
//            queryASSenderMsgRecord.setCreateTime(new Date(beforeOf));
//        } else {
//            queryASSenderMsgRecord.setCreateTime(null);
//        }
//        queryASSenderMsgRecord.setTableName(Constants.WEB_MSG_RECORD_PREFIX + (objUid % 10));//表名
//        queryASSenderMsgRecord.setLimitSize(limitSize);
//        List<WebMsgRecord> asSenderMsgRecord = webMsgRecordService.selectWebMsgRecordList(queryASSenderMsgRecord);
//
//        //查询作为接收方的消息
//        WebMsgRecord queryASReceiverMsgRecord = new WebMsgRecord();
//        queryASReceiverMsgRecord.setSenderUid(objUid);//发送者ID
//        queryASReceiverMsgRecord.setReceiverUid(userId);//接收者ID
//        //添加查询时间条件 todo 测试按时间查询是否正常
//        if (beforeOf != null && beforeOf != 0) {
//            queryASReceiverMsgRecord.setCreateTime(new Date(beforeOf));
//        } else {
//            queryASReceiverMsgRecord.setCreateTime(null);
//        }
//        queryASReceiverMsgRecord.setTableName(Constants.WEB_MSG_RECORD_PREFIX + (userId % 10));//表名
//        queryASSenderMsgRecord.setLimitSize(limitSize);
//        List<WebMsgRecord> asReceiverMsgRecord = webMsgRecordService.selectWebMsgRecordList(queryASReceiverMsgRecord);
//
//        //将集合按时间排序，并裁剪
//        asSenderMsgRecord.addAll(asReceiverMsgRecord);//将两个集合合并
//        asSenderMsgRecord.sort((t1, t2) -> t2.getCreateTime().compareTo(t1.getCreateTime()));//按时间排序（降序） todo 测试排序
//        if (asSenderMsgRecord.size() > TableSupport.buildPageRequest().getPageSize()) {
//            asSenderMsgRecord = asSenderMsgRecord.subList(limitSize - pageDomain.getPageSize(), limitSize);//裁剪集合 todo 测试裁剪
//        }
        WebMsgRecord queryMsgRecord = new WebMsgRecord();
        queryMsgRecord.setSenderUid(userId);//发送者ID
        queryMsgRecord.setReceiverUid(objUid);//接收者ID
        PageUtils.startPage();
        //添加查询时间条件
        if (beforeOf != null && beforeOf != 0) {
            queryMsgRecord.setCreateTime(new Date(beforeOf));
        } else {
            queryMsgRecord.setCreateTime(null);
        }
        PageUtils.startPage();
        return AjaxResult.success(webMsgRecordService.selectWebMsgRecordList(queryMsgRecord));
    }

    /**
     * 点击了某个对话框，修改已读状态
     * @param userId 用户ID
     * @param objUid 聊天对象ID
     */
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult updateReadStatus(Long userId, Long objUid){
        //查询数据
        WebMsgList queryMsgList = new WebMsgList();
        queryMsgList.setUserId(userId);//用户ID
        queryMsgList.setObjUid(objUid);//聊天对象用户ID
        List<WebMsgList> queriedMsgListLis = webMsgListService.selectWebMsgListList(queryMsgList);
        if (queriedMsgListLis.size() == 0) {
            return AjaxResult.error(5001,"Database not found data");
        }

        //修改数据
        WebMsgList updateMsgList = queriedMsgListLis.get(0);
        updateMsgList.setUnRead(0L);//是否有未读消息（0无，1有）
        webMsgListService.updateWebMsgList(updateMsgList);

        return AjaxResult.success();
    }

    //todo 用户删除消息记录
}
