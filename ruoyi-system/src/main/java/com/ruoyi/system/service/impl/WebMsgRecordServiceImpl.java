package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WebMsgRecordMapper;
import com.ruoyi.system.domain.WebMsgRecord;
import com.ruoyi.system.service.IWebMsgRecordService;

/**
 * 消息记录表Service业务层处理
 * 
 * @author kevin
 * @date 2022-05-09
 */
@Service
public class WebMsgRecordServiceImpl implements IWebMsgRecordService 
{
    @Autowired
    private WebMsgRecordMapper webMsgRecordMapper;

    /**
     * 查询消息记录表
     * 
     * @param id 消息记录表主键
     * @return 消息记录表
     */
    @Override
    public WebMsgRecord selectWebMsgRecordById(String id)
    {
        return webMsgRecordMapper.selectWebMsgRecordById(id);
    }

    /**
     * 查询消息记录表列表
     * 
     * @param webMsgRecord 消息记录表
     * @return 消息记录表
     */
    @Override
    public List<WebMsgRecord> selectWebMsgRecordList(WebMsgRecord webMsgRecord)
    {
        return webMsgRecordMapper.selectWebMsgRecordList(webMsgRecord);
    }

    /**
     * 新增消息记录表
     * 
     * @param webMsgRecord 消息记录表
     * @return 结果
     */
    @Override
    public int insertWebMsgRecord(WebMsgRecord webMsgRecord)
    {
        return webMsgRecordMapper.insertWebMsgRecord(webMsgRecord);
    }

    /**
     * 修改消息记录表
     * 
     * @param webMsgRecord 消息记录表
     * @return 结果
     */
    @Override
    public int updateWebMsgRecord(WebMsgRecord webMsgRecord)
    {
        return webMsgRecordMapper.updateWebMsgRecord(webMsgRecord);
    }

    /**
     * 批量删除消息记录表
     * 
     * @param ids 需要删除的消息记录表主键
     * @return 结果
     */
    @Override
    public int deleteWebMsgRecordByIds(String[] ids)
    {
        return webMsgRecordMapper.deleteWebMsgRecordByIds(ids);
    }

    /**
     * 删除消息记录表信息
     * 
     * @param id 消息记录表主键
     * @return 结果
     */
    @Override
    public int deleteWebMsgRecordById(String id)
    {
        return webMsgRecordMapper.deleteWebMsgRecordById(id);
    }
}
