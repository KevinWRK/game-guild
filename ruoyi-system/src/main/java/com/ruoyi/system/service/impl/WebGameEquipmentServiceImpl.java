package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WebGameEquipmentMapper;
import com.ruoyi.system.domain.WebGameEquipment;
import com.ruoyi.system.service.IWebGameEquipmentService;

/**
 * 游戏装备Service业务层处理
 * 
 * @author kevin
 * @date 2022-05-08
 */
@Service
public class WebGameEquipmentServiceImpl implements IWebGameEquipmentService 
{
    @Autowired
    private WebGameEquipmentMapper webGameEquipmentMapper;

    /**
     * 查询游戏装备
     * 
     * @param id 游戏装备主键
     * @return 游戏装备
     */
    @Override
    public WebGameEquipment selectWebGameEquipmentById(String id)
    {
        return webGameEquipmentMapper.selectWebGameEquipmentById(id);
    }

    /**
     * 查询游戏装备列表
     * 
     * @param webGameEquipment 游戏装备
     * @return 游戏装备
     */
    @Override
    public List<WebGameEquipment> selectWebGameEquipmentList(WebGameEquipment webGameEquipment)
    {
        return webGameEquipmentMapper.selectWebGameEquipmentList(webGameEquipment);
    }

    /**
     * 新增游戏装备
     * 
     * @param webGameEquipment 游戏装备
     * @return 结果
     */
    @Override
    public int insertWebGameEquipment(WebGameEquipment webGameEquipment)
    {
        webGameEquipment.setCreateTime(DateUtils.getNowDate());
        return webGameEquipmentMapper.insertWebGameEquipment(webGameEquipment);
    }

    /**
     * 修改游戏装备
     * 
     * @param webGameEquipment 游戏装备
     * @return 结果
     */
    @Override
    public int updateWebGameEquipment(WebGameEquipment webGameEquipment)
    {
        webGameEquipment.setUpdateTime(DateUtils.getNowDate());
        return webGameEquipmentMapper.updateWebGameEquipment(webGameEquipment);
    }

    /**
     * 批量删除游戏装备
     * 
     * @param ids 需要删除的游戏装备主键
     * @return 结果
     */
    @Override
    public int deleteWebGameEquipmentByIds(String[] ids)
    {
        return webGameEquipmentMapper.deleteWebGameEquipmentByIds(ids);
    }

    /**
     * 删除游戏装备信息
     * 
     * @param id 游戏装备主键
     * @return 结果
     */
    @Override
    public int deleteWebGameEquipmentById(String id)
    {
        return webGameEquipmentMapper.deleteWebGameEquipmentById(id);
    }
}
