package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WebGameMapper;
import com.ruoyi.system.domain.WebGame;
import com.ruoyi.system.service.IWebGameService;

/**
 * 游戏Service业务层处理
 * 
 * @author kevin
 * @date 2022-05-08
 */
@Service
public class WebGameServiceImpl implements IWebGameService 
{
    @Autowired
    private WebGameMapper webGameMapper;

    /**
     * 查询游戏
     * 
     * @param id 游戏主键
     * @return 游戏
     */
    @Override
    public WebGame selectWebGameById(String id)
    {
        return webGameMapper.selectWebGameById(id);
    }

    /**
     * 查询游戏列表
     * 
     * @param webGame 游戏
     * @return 游戏
     */
    @Override
    public List<WebGame> selectWebGameList(WebGame webGame)
    {
        return webGameMapper.selectWebGameList(webGame);
    }

    /**
     * 新增游戏
     * 
     * @param webGame 游戏
     * @return 结果
     */
    @Override
    public int insertWebGame(WebGame webGame)
    {
        webGame.setCreateTime(DateUtils.getNowDate());
        return webGameMapper.insertWebGame(webGame);
    }

    /**
     * 修改游戏
     * 
     * @param webGame 游戏
     * @return 结果
     */
    @Override
    public int updateWebGame(WebGame webGame)
    {
        return webGameMapper.updateWebGame(webGame);
    }

    /**
     * 批量删除游戏
     * 
     * @param ids 需要删除的游戏主键
     * @return 结果
     */
    @Override
    public int deleteWebGameByIds(String[] ids)
    {
        return webGameMapper.deleteWebGameByIds(ids);
    }

    /**
     * 删除游戏信息
     * 
     * @param id 游戏主键
     * @return 结果
     */
    @Override
    public int deleteWebGameById(String id)
    {
        return webGameMapper.deleteWebGameById(id);
    }
}
