package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.WebGameEquipment;

/**
 * 游戏装备Service接口
 * 
 * @author kevin
 * @date 2022-05-08
 */
public interface IWebGameEquipmentService 
{
    /**
     * 查询游戏装备
     * 
     * @param id 游戏装备主键
     * @return 游戏装备
     */
    public WebGameEquipment selectWebGameEquipmentById(String id);

    /**
     * 查询游戏装备列表
     * 
     * @param webGameEquipment 游戏装备
     * @return 游戏装备集合
     */
    public List<WebGameEquipment> selectWebGameEquipmentList(WebGameEquipment webGameEquipment);

    /**
     * 新增游戏装备
     * 
     * @param webGameEquipment 游戏装备
     * @return 结果
     */
    public int insertWebGameEquipment(WebGameEquipment webGameEquipment);

    /**
     * 修改游戏装备
     * 
     * @param webGameEquipment 游戏装备
     * @return 结果
     */
    public int updateWebGameEquipment(WebGameEquipment webGameEquipment);

    /**
     * 批量删除游戏装备
     * 
     * @param ids 需要删除的游戏装备主键集合
     * @return 结果
     */
    public int deleteWebGameEquipmentByIds(String[] ids);

    /**
     * 删除游戏装备信息
     * 
     * @param id 游戏装备主键
     * @return 结果
     */
    public int deleteWebGameEquipmentById(String id);
}
