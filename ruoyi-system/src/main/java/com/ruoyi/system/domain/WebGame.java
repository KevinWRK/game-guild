package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 游戏对象 web_game
 * 
 * @author kevin
 * @date 2022-05-08
 */
public class WebGame extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 游戏名 */
    @Excel(name = "游戏名")
    private String gameName;

    /** 游戏详情/介绍 */
    @Excel(name = "游戏详情/介绍")
    private String gameDetails;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String img1;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String img2;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String img3;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String img4;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String img5;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String img6;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String img7;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String img8;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String img9;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String img10;

    /** 1上架2下架 */
    @Excel(name = "1上架2下架")
    private Long status;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long createAdminId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date upadteTime;

    /** 0未，1删 */
    @Excel(name = "0未，1删")
    private Long isDelete;

    /** 是否显示价格，0不显示价格，1显示价格 */
    @Excel(name = "排序字段（数字越大显示越靠前）", readConverterExp = "排序字段（数字越大显示越靠前）")
    private String extra1;

    /** 是否允许用户上传,0不能上传，1可以上传 */
    @Excel(name = "是否允许用户上传", readConverterExp = "0不能上传，1可以上传")
    private String extra2;

    /** 是否显示价格，0不显示价格，1显示价格 */
    @Excel(name = "是否显示价格", readConverterExp = "0不显示价格，1显示价格")
    private String extra3;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String extra4;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String extra5;

    /** 当前游戏的装备数量 */
    private int equipmentNum;

    public int getEquipmentNum() {
        return equipmentNum;
    }

    public void setEquipmentNum(int equipmentNum) {
        this.equipmentNum = equipmentNum;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGameName(String gameName) 
    {
        this.gameName = gameName;
    }

    public String getGameName() 
    {
        return gameName;
    }
    public void setGameDetails(String gameDetails) 
    {
        this.gameDetails = gameDetails;
    }

    public String getGameDetails() 
    {
        return gameDetails;
    }
    public void setImg1(String img1) 
    {
        this.img1 = img1;
    }

    public String getImg1() 
    {
        return img1;
    }
    public void setImg2(String img2) 
    {
        this.img2 = img2;
    }

    public String getImg2() 
    {
        return img2;
    }
    public void setImg3(String img3) 
    {
        this.img3 = img3;
    }

    public String getImg3() 
    {
        return img3;
    }
    public void setImg4(String img4) 
    {
        this.img4 = img4;
    }

    public String getImg4() 
    {
        return img4;
    }
    public void setImg5(String img5) 
    {
        this.img5 = img5;
    }

    public String getImg5() 
    {
        return img5;
    }
    public void setImg6(String img6) 
    {
        this.img6 = img6;
    }

    public String getImg6() 
    {
        return img6;
    }
    public void setImg7(String img7) 
    {
        this.img7 = img7;
    }

    public String getImg7() 
    {
        return img7;
    }
    public void setImg8(String img8) 
    {
        this.img8 = img8;
    }

    public String getImg8() 
    {
        return img8;
    }
    public void setImg9(String img9) 
    {
        this.img9 = img9;
    }

    public String getImg9() 
    {
        return img9;
    }
    public void setImg10(String img10) 
    {
        this.img10 = img10;
    }

    public String getImg10() 
    {
        return img10;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setCreateAdminId(Long createAdminId) 
    {
        this.createAdminId = createAdminId;
    }

    public Long getCreateAdminId() 
    {
        return createAdminId;
    }
    public void setUpadteTime(Date upadteTime) 
    {
        this.upadteTime = upadteTime;
    }

    public Date getUpadteTime() 
    {
        return upadteTime;
    }
    public void setIsDelete(Long isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Long getIsDelete() 
    {
        return isDelete;
    }
    public void setExtra1(String extra1) 
    {
        this.extra1 = extra1;
    }

    public String getExtra1() 
    {
        return extra1;
    }
    public void setExtra2(String extra2) 
    {
        this.extra2 = extra2;
    }

    public String getExtra2() 
    {
        return extra2;
    }
    public void setExtra3(String extra3) 
    {
        this.extra3 = extra3;
    }

    public String getExtra3() 
    {
        return extra3;
    }
    public void setExtra4(String extra4) 
    {
        this.extra4 = extra4;
    }

    public String getExtra4() 
    {
        return extra4;
    }
    public void setExtra5(String extra5) 
    {
        this.extra5 = extra5;
    }

    public String getExtra5() 
    {
        return extra5;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("gameName", getGameName())
            .append("gameDetails", getGameDetails())
            .append("img1", getImg1())
            .append("img2", getImg2())
            .append("img3", getImg3())
            .append("img4", getImg4())
            .append("img5", getImg5())
            .append("img6", getImg6())
            .append("img7", getImg7())
            .append("img8", getImg8())
            .append("img9", getImg9())
            .append("img10", getImg10())
            .append("status", getStatus())
            .append("createAdminId", getCreateAdminId())
            .append("createTime", getCreateTime())
            .append("upadteTime", getUpadteTime())
            .append("isDelete", getIsDelete())
            .append("extra1", getExtra1())
            .append("extra2", getExtra2())
            .append("extra3", getExtra3())
            .append("extra4", getExtra4())
            .append("extra5", getExtra5())
            .toString();
    }
}
