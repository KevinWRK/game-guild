package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 消息记录表对象 web_msg_record
 * 
 * @author kevin
 * @date 2022-05-09
 */
public class WebMsgRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long senderUid;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long receiverUid;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String content;

    /** 消息类型（1文字，2图片，3文件，4语言） */
    @Excel(name = "消息类型", readConverterExp = "1=文字，2图片，3文件，4语言")
    private Long msgType;

    /** 已读（0未读，1已读） */
    @Excel(name = "已读", readConverterExp = "0=未读，1已读")
    private Long isRead;

    /** 发送者删除消息记录（0未删，1已删） */
    @Excel(name = "发送者删除消息记录", readConverterExp = "0=未删，1已删")
    private Long senderDelete;

    /** 接收者删除消息记录（0未删，1已删） */
    @Excel(name = "接收者删除消息记录", readConverterExp = "0=未删，1已删")
    private Long receiverDelete;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String extra1;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String extra2;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String extra3;

    private int limitSize;

    private String tableName;

    private Date beforeOfDate;

    public int getLimitSize() {
        return limitSize;
    }

    public void setLimitSize(int limitSize) {
        this.limitSize = limitSize;
    }

    public Date getBeforeOfDate() {
        return beforeOfDate;
    }

    public void setBeforeOfDate(Date beforeOfDate) {
        this.beforeOfDate = beforeOfDate;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSenderUid(Long senderUid) 
    {
        this.senderUid = senderUid;
    }

    public Long getSenderUid() 
    {
        return senderUid;
    }
    public void setReceiverUid(Long receiverUid) 
    {
        this.receiverUid = receiverUid;
    }

    public Long getReceiverUid() 
    {
        return receiverUid;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setMsgType(Long msgType) 
    {
        this.msgType = msgType;
    }

    public Long getMsgType() 
    {
        return msgType;
    }
    public void setIsRead(Long isRead) 
    {
        this.isRead = isRead;
    }

    public Long getIsRead() 
    {
        return isRead;
    }
    public void setSenderDelete(Long senderDelete) 
    {
        this.senderDelete = senderDelete;
    }

    public Long getSenderDelete() 
    {
        return senderDelete;
    }
    public void setReceiverDelete(Long receiverDelete) 
    {
        this.receiverDelete = receiverDelete;
    }

    public Long getReceiverDelete() 
    {
        return receiverDelete;
    }
    public void setExtra1(String extra1) 
    {
        this.extra1 = extra1;
    }

    public String getExtra1() 
    {
        return extra1;
    }
    public void setExtra2(String extra2) 
    {
        this.extra2 = extra2;
    }

    public String getExtra2() 
    {
        return extra2;
    }
    public void setExtra3(String extra3) 
    {
        this.extra3 = extra3;
    }

    public String getExtra3() 
    {
        return extra3;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("senderUid", getSenderUid())
            .append("receiverUid", getReceiverUid())
            .append("content", getContent())
            .append("msgType", getMsgType())
            .append("isRead", getIsRead())
            .append("senderDelete", getSenderDelete())
            .append("receiverDelete", getReceiverDelete())
            .append("createTime", getCreateTime())
            .append("extra1", getExtra1())
            .append("extra2", getExtra2())
            .append("extra3", getExtra3())
            .toString();
    }
}
