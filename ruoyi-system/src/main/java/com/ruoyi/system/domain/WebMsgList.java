package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 消息记录列表对象 web_msg_list
 * 
 * @author kevin
 * @date 2022-05-09
 */
public class WebMsgList extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 聊天对象用户ID */
    @Excel(name = "聊天对象用户ID")
    private Long objUid;

    /** 是否有未读消息（0无，1有） */
    @Excel(name = "是否有未读消息", readConverterExp = "0=无，1有")
    private Long unRead;

    /** 未读消息数量 */
    @Excel(name = "未读消息数量")
    private Long unReadNum;

    /** 最后一条消息的时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后一条消息的时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastTime;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String extra1;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String extra2;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String extra3;

    /** 头像 */
    private String headImg;

    /** 昵称 */
    private String nickName;

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setObjUid(Long objUid) 
    {
        this.objUid = objUid;
    }

    public Long getObjUid() 
    {
        return objUid;
    }
    public void setUnRead(Long unRead) 
    {
        this.unRead = unRead;
    }

    public Long getUnRead() 
    {
        return unRead;
    }
    public void setUnReadNum(Long unReadNum) 
    {
        this.unReadNum = unReadNum;
    }

    public Long getUnReadNum() 
    {
        return unReadNum;
    }
    public void setLastTime(Date lastTime) 
    {
        this.lastTime = lastTime;
    }

    public Date getLastTime() 
    {
        return lastTime;
    }
    public void setExtra1(String extra1) 
    {
        this.extra1 = extra1;
    }

    public String getExtra1() 
    {
        return extra1;
    }
    public void setExtra2(String extra2) 
    {
        this.extra2 = extra2;
    }

    public String getExtra2() 
    {
        return extra2;
    }
    public void setExtra3(String extra3) 
    {
        this.extra3 = extra3;
    }

    public String getExtra3() 
    {
        return extra3;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("objUid", getObjUid())
            .append("unRead", getUnRead())
            .append("unReadNum", getUnReadNum())
            .append("lastTime", getLastTime())
            .append("extra1", getExtra1())
            .append("extra2", getExtra2())
            .append("extra3", getExtra3())
            .toString();
    }
}
