package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户信息对象 web_user
 * 
 * @author kevin
 * @date 2022-05-08
 */
public class WebUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** 登录名，账号 */
    @Excel(name = "登录名，账号")
    private String username;

    /** 登录密码 */
    private String password;

    /** 资金密码 */
    private String fundPassword;

    /** 昵称 */
    @Excel(name = "昵称")
    private String nickName;

    /** 头像 */
    @Excel(name = "头像")
    private String headImg;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobile;

    /** 手机验证（0未通过，1通过，2正在审核，3审核失败） */
    @Excel(name = "手机验证", readConverterExp = "0=未通过，1通过，2正在审核，3审核失败")
    private Long mobileVerify;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 邮箱验证（0未通过，1通过，2正在审核，3审核失败） */
    @Excel(name = "邮箱验证", readConverterExp = "0=未通过，1通过，2正在审核，3审核失败")
    private Long emailVerify;

    /** 谷歌验证器私钥 */
    @Excel(name = "谷歌验证器私钥")
    private String googleKey;

    /** 谷歌验证（0未通过，1通过，2正在审核，3审核失败） */
    @Excel(name = "谷歌验证", readConverterExp = "0=未通过，1通过，2正在审核，3审核失败")
    private Long googleVerify;

    /** 身份证正面 */
    @Excel(name = "身份证正面")
    private String idCardFrontImg;

    /** 身份证背面 */
    @Excel(name = "身份证背面")
    private String idCardBackImg;

    /** 手持身份证 */
    @Excel(name = "手持身份证")
    private String idCardHold;

    /** 证件名 */
    @Excel(name = "证件名")
    private String realName;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idCardNumber;

    /** 实名验证（0未通过，1通过，2正在审核，3审核失败） */
    @Excel(name = "实名验证", readConverterExp = "0=未通过，1通过，2正在审核，3审核失败")
    private Long realNameVerify;

    /** 保证金(usdt) */
    @Excel(name = "保证金(usdt)")
    private String deposit;

    /** 保证金验证（0未通过，1通过，2正在审核，3审核失败） */
    @Excel(name = "保证金验证", readConverterExp = "0=未通过，1通过，2正在审核，3审核失败")
    private Long depositVerify;

    /** 计价方式id */
    @Excel(name = "计价方式id")
    private Long pricingMethod;

    /** 推荐人uid */
    @Excel(name = "推荐人uid")
    private Long parentUid;

    /** 已推广人数 */
    @Excel(name = "已推广人数")
    private Long numberOfPromoters;

    /** 是否加V（0未加V，1已加V） */
    @Excel(name = "是否加V", readConverterExp = "0=未加V，1已加V")
    private Long isVip;

    /** 账号状态（1正常，2注销，3封号） */
    @Excel(name = "账号状态", readConverterExp = "1=正常，2注销，3封号")
    private Long accountStatus;

    /** 账号类型（0普通账户，1测试账号） */
    @Excel(name = "账号类型", readConverterExp = "0=普通账户，1测试账号")
    private Long accountType;

    /** 账号地区 */
    @Excel(name = "账号地区")
    private String accountArea;

    /** 登录ip */
    @Excel(name = "登录ip")
    private String loginip;

    /** 注册ip */
    @Excel(name = "注册ip")
    private String registerip;

    /** $column.columnComment */
    private String extra1;

    /** $column.columnComment */
    private String extra2;

    /** 账号类型：1正式， 2测试 */
    private String extra3;

    /** 拓展字段 */
    private String extra4;

    /** 拓展字段 */
    private String extra5;

    /** 拓展字段 */
    private String extra6;

    /** 拓展字段 */
    private String extra7;

    /** 拓展字段 */
    private String extra8;

    /** 拓展字段 */
    private String extra9;

    /** 拓展字段 */
    private String extra10;

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setFundPassword(String fundPassword) 
    {
        this.fundPassword = fundPassword;
    }

    public String getFundPassword() 
    {
        return fundPassword;
    }
    public void setNickName(String nickName) 
    {
        this.nickName = nickName;
    }

    public String getNickName() 
    {
        return nickName;
    }
    public void setHeadImg(String headImg) 
    {
        this.headImg = headImg;
    }

    public String getHeadImg() 
    {
        return headImg;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setMobileVerify(Long mobileVerify) 
    {
        this.mobileVerify = mobileVerify;
    }

    public Long getMobileVerify() 
    {
        return mobileVerify;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setEmailVerify(Long emailVerify) 
    {
        this.emailVerify = emailVerify;
    }

    public Long getEmailVerify() 
    {
        return emailVerify;
    }
    public void setGoogleKey(String googleKey) 
    {
        this.googleKey = googleKey;
    }

    public String getGoogleKey() 
    {
        return googleKey;
    }
    public void setGoogleVerify(Long googleVerify) 
    {
        this.googleVerify = googleVerify;
    }

    public Long getGoogleVerify() 
    {
        return googleVerify;
    }
    public void setIdCardFrontImg(String idCardFrontImg) 
    {
        this.idCardFrontImg = idCardFrontImg;
    }

    public String getIdCardFrontImg() 
    {
        return idCardFrontImg;
    }
    public void setIdCardBackImg(String idCardBackImg) 
    {
        this.idCardBackImg = idCardBackImg;
    }

    public String getIdCardBackImg() 
    {
        return idCardBackImg;
    }
    public void setIdCardHold(String idCardHold) 
    {
        this.idCardHold = idCardHold;
    }

    public String getIdCardHold() 
    {
        return idCardHold;
    }
    public void setRealName(String realName) 
    {
        this.realName = realName;
    }

    public String getRealName() 
    {
        return realName;
    }
    public void setIdCardNumber(String idCardNumber) 
    {
        this.idCardNumber = idCardNumber;
    }

    public String getIdCardNumber() 
    {
        return idCardNumber;
    }
    public void setRealNameVerify(Long realNameVerify) 
    {
        this.realNameVerify = realNameVerify;
    }

    public Long getRealNameVerify() 
    {
        return realNameVerify;
    }
    public void setDeposit(String deposit) 
    {
        this.deposit = deposit;
    }

    public String getDeposit() 
    {
        return deposit;
    }
    public void setDepositVerify(Long depositVerify) 
    {
        this.depositVerify = depositVerify;
    }

    public Long getDepositVerify() 
    {
        return depositVerify;
    }
    public void setPricingMethod(Long pricingMethod) 
    {
        this.pricingMethod = pricingMethod;
    }

    public Long getPricingMethod() 
    {
        return pricingMethod;
    }
    public void setParentUid(Long parentUid) 
    {
        this.parentUid = parentUid;
    }

    public Long getParentUid() 
    {
        return parentUid;
    }
    public void setNumberOfPromoters(Long numberOfPromoters) 
    {
        this.numberOfPromoters = numberOfPromoters;
    }

    public Long getNumberOfPromoters() 
    {
        return numberOfPromoters;
    }
    public void setIsVip(Long isVip) 
    {
        this.isVip = isVip;
    }

    public Long getIsVip() 
    {
        return isVip;
    }
    public void setAccountStatus(Long accountStatus) 
    {
        this.accountStatus = accountStatus;
    }

    public Long getAccountStatus() 
    {
        return accountStatus;
    }
    public void setAccountType(Long accountType) 
    {
        this.accountType = accountType;
    }

    public Long getAccountType() 
    {
        return accountType;
    }
    public void setAccountArea(String accountArea) 
    {
        this.accountArea = accountArea;
    }

    public String getAccountArea() 
    {
        return accountArea;
    }
    public void setLoginip(String loginip) 
    {
        this.loginip = loginip;
    }

    public String getLoginip() 
    {
        return loginip;
    }
    public void setRegisterip(String registerip) 
    {
        this.registerip = registerip;
    }

    public String getRegisterip() 
    {
        return registerip;
    }
    public void setExtra1(String extra1) 
    {
        this.extra1 = extra1;
    }

    public String getExtra1() 
    {
        return extra1;
    }
    public void setExtra2(String extra2) 
    {
        this.extra2 = extra2;
    }

    public String getExtra2() 
    {
        return extra2;
    }
    public void setExtra3(String extra3) 
    {
        this.extra3 = extra3;
    }

    public String getExtra3() 
    {
        return extra3;
    }
    public void setExtra4(String extra4) 
    {
        this.extra4 = extra4;
    }

    public String getExtra4() 
    {
        return extra4;
    }
    public void setExtra5(String extra5) 
    {
        this.extra5 = extra5;
    }

    public String getExtra5() 
    {
        return extra5;
    }
    public void setExtra6(String extra6) 
    {
        this.extra6 = extra6;
    }

    public String getExtra6() 
    {
        return extra6;
    }
    public void setExtra7(String extra7) 
    {
        this.extra7 = extra7;
    }

    public String getExtra7() 
    {
        return extra7;
    }
    public void setExtra8(String extra8) 
    {
        this.extra8 = extra8;
    }

    public String getExtra8() 
    {
        return extra8;
    }
    public void setExtra9(String extra9) 
    {
        this.extra9 = extra9;
    }

    public String getExtra9() 
    {
        return extra9;
    }
    public void setExtra10(String extra10) 
    {
        this.extra10 = extra10;
    }

    public String getExtra10() 
    {
        return extra10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("fundPassword", getFundPassword())
            .append("nickName", getNickName())
            .append("headImg", getHeadImg())
            .append("mobile", getMobile())
            .append("mobileVerify", getMobileVerify())
            .append("email", getEmail())
            .append("emailVerify", getEmailVerify())
            .append("googleKey", getGoogleKey())
            .append("googleVerify", getGoogleVerify())
            .append("idCardFrontImg", getIdCardFrontImg())
            .append("idCardBackImg", getIdCardBackImg())
            .append("idCardHold", getIdCardHold())
            .append("realName", getRealName())
            .append("idCardNumber", getIdCardNumber())
            .append("realNameVerify", getRealNameVerify())
            .append("deposit", getDeposit())
            .append("depositVerify", getDepositVerify())
            .append("pricingMethod", getPricingMethod())
            .append("parentUid", getParentUid())
            .append("numberOfPromoters", getNumberOfPromoters())
            .append("isVip", getIsVip())
            .append("accountStatus", getAccountStatus())
            .append("accountType", getAccountType())
            .append("accountArea", getAccountArea())
            .append("loginip", getLoginip())
            .append("registerip", getRegisterip())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("extra1", getExtra1())
            .append("extra2", getExtra2())
            .append("extra3", getExtra3())
            .append("extra4", getExtra4())
            .append("extra5", getExtra5())
            .append("extra6", getExtra6())
            .append("extra7", getExtra7())
            .append("extra8", getExtra8())
            .append("extra9", getExtra9())
            .append("extra10", getExtra10())
            .toString();
    }
}
